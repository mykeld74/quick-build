var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var del = require('del');

gulp.task('serve', ['sass'], function() {

  browserSync.init({
    server: {
      baseDir: "./"
    }
  });

  gulp.watch("assets/stylesheets/*.scss", ['sass']);
  gulp.watch("*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("assets/stylesheets/*.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'],
    }))
    .pipe(csscomb())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('/maps'))
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.stream());
});



gulp.task('clean', function(cb) {
  del(['dist/**'], cb);
});

gulp.task('build_files', function() {
  setTimeout(function() {
    return gulp.src(['*/*', '*.*', '*/*/*', '!gulpfile.js', '!package.json', '!assets/stylesheets/**', '!node_modules/**'])
      .pipe(gulp.dest('dist'));
  }, 250);
});

gulp.task('build', ['clean', 'build_files'], function() {});

gulp.task('default', ['serve']);
